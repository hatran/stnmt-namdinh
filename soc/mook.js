var mook = {
    sobn: function () {
        let raw = `
        Ban quản lý các KCN
        Sở Công thương
        Sở Khoa học và Công nghệ
        Sở Lao động TBXH
        Sở Nông nghiệp và PTNT
        Sở Tài nguyên và Môi trường
        Sở Thông tin và Truyền thông`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `Sở Văn hóa, Thể thao
        Sở Y tế
        Văn phòng UBND TP
        Sở Giao thông Vận tải
        Sở Ngoại vụ
        Sở Nội vụ
        Sở Tài chính
        Sở Tư pháp
        Sở Xây dựng
        Thanh tra TP
        Sở Kế hoạch và Đầu tư`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `UBND TP Nam Định
                UBND Huyện Giao Thủy‎
                UBND Huyện Hải Hậu‎
                UBND Huyện Mỹ Lộc‎ 
                Sở Tư pháp
                Sở Tài chính
                Sở Y tế
                Sở Xây dựng
                Sở Ngoại vụ
                Sở Nội vụ
                Thanh tra TP`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `Văn phòng đăng ký đất đai
        TT quan trắc - kỹ thuật môi trường
        TT phát triển quỹ đất
        TT công nghệ thông tin - lưu trữ
        Quỹ bảo vệ môi trường
        Văn phòng sở
        Thanh tra sở`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `Văn phòng đăng ký đất đai
        TT quan trắc - kỹ thuật môi trường
        TT phát triển quỹ đất
        TT công nghệ thông tin - lưu trữ
        Quỹ bảo vệ môi trường
        Văn phòng sở
        Thanh tra sở`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
